import { Time } from './Time';

export interface ScheduleData{
  time: Time;
  volume: number;
}
