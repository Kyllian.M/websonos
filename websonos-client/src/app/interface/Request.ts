export interface Request{
    id_music: number; // id de la musique qui fait la requête (pause,start,...) nullable
    status: string; // le nom de la requête
    timestamp: number; // la position du lecteur en seconde à l'instant de la requête
    volume: number; // nouveau volume set
}
