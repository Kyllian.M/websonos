export interface Metadata{
  filename: string; // nom du fichier
  title: string; // titre (nullable)
  artist: string; // artiste (nullable)
  composer: string;
  genre: string; // genre de la music (nullable)
  album: string; // album (nullable)
  duration: string; // durée du fichier
  img_b64: string;
}
