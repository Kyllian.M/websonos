import { Music } from './Music';

export interface Playlist{
  id: number; // id de la playlist
  date_create: Date; // date de creation de la playlist
  name: string; // nom de la playlist
  music: Array<Music>; // tableau de music appartenant à cette playlist

}
