import { Metadata } from './Metadata';

export interface Music{
  metadata: Metadata; // Metadata de la music
  id: number; // id de la musique
  url: string; // url de l'image
  timestamp: number; // position de lecture actuelle en seconde
  volume: number; // volume actuel de la music
  playing: boolean; // savoir l'etat de la music (true : play , false : pause);
}
