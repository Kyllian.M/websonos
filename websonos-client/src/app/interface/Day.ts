import { ScheduleData } from './ScheduleData';

export interface Day{
  start_list: Array<ScheduleData>;
  stop_list: Array<ScheduleData>;
}
