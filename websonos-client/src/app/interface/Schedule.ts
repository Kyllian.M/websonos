import { Day } from './Day';

export interface Schedule{
  days: Array<Day>;
}
