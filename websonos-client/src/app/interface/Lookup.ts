export interface Lookup{
    title: string; // intitulé de la recherche
    limit: number; // limite du nombre de résultat
}
