import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Schedule } from '../interface/Schedule';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  ScheduleApi = environment.apiUrl + 'schedule';
  PostScheduleApi = environment.apiUrl + 'postschedule';
  scheduleSubjects: BehaviorSubject<Schedule> = new BehaviorSubject<Schedule>(null);
  constructor(private http: HttpClient) {
    this.getRequestSchedule();
   }

   /**
    * Permet de faire une requête GET pour récupérer le planning de lancement et d'arret de la music
    * @return void
    */
   getRequestSchedule(): void {
     this.http.get<Schedule>(this.ScheduleApi)
     .subscribe(
       data => { this.scheduleSubjects.next(data);
    });
   }

   /**
    * Permet de faire une requête POST contenant le nouveau planning (UPDATE/DELETE/ADD)
    * @param schedule nouveau planning
    * @return void
    */
   postRequestSchedule(schedule: Schedule): void{
    this.http.post(this.PostScheduleApi, schedule)
      .subscribe(() => {}, error =>  console.error('There was an error at PostSchedule!', error));
   }
   /**
    * Renvoit l'observable du planning
    * @return Observable<Schedule>
    */
   getSchedule(): Observable<Schedule> {
     return this.scheduleSubjects.asObservable();
   }
}
