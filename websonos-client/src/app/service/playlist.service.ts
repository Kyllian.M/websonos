import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Playlist } from '../interface/Playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  playlistSubject: BehaviorSubject<Playlist> = new BehaviorSubject<Playlist>(null);
  playlistApi = environment.apiUrl + 'playlist';
  constructor(private http: HttpClient) {

  }
  /**
   * Permet d'effectuer une requete GET pour récupérer une playlist
   * avec un offset et une limit pour savoir le nombre de music à récupérer
   * @param offset offset du début de la playlist
   * @param limit nombre limite de music à récupérer
   * @return any
   */
  getRequestPlaylist(offset: string, limit: string): any{
    return this.http.get<Playlist>(this.playlistApi + '?offset=' + offset + '&' + 'limit=' + limit)
    .subscribe(data => this.playlistSubject.next(data));
  }

  /**
   * Renvoit l'observable de la playlist
   * @return Observable<Playlist>
   */
  getPlaylist(): Observable<Playlist>{
    return this.playlistSubject.asObservable();
  }




}
