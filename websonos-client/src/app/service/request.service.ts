import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Request } from '../interface/Request';
@Injectable({
  providedIn: 'root'
})
export class RequestService {
  requestApi = environment.apiUrl + 'command' ;
  request: Request = {
    id_music: null,
    status: '',
    timestamp: null,
    volume: null,
  };
  constructor(private http: HttpClient) { }

  /**
   * Permet d'envoyer des requêtes GET pour éffectuer des commandes pour le lecteur
   * @param status intitulé de la commande (play/stop/previous/skip)
   * @param volume volume de la music (nullable)
   * @param musicID id de la music (nullable)
   * @param timestamp timestamp de la music (nullable)
   * @return void
   */
  sendRequest(status: string, volume: number = null, musicID: number= null, timestamp: number= null): void{
    this.request.id_music = musicID;
    this.request.timestamp = timestamp;
    this.request.status = status;
    this.request.volume = volume;
    this.http.post(this.requestApi, this.request).subscribe(() => {},
        error => {
          console.error('There was an error at request!', error);
      }
    );
  }

}
