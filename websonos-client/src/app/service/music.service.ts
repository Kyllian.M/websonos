import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Music } from '../interface/Music';


@Injectable({
  providedIn: 'root'
})
export class MusicService {

   musicSubjects: BehaviorSubject<Music> = new BehaviorSubject<Music>(null);
   nmusicSubjects: BehaviorSubject<Music> = new BehaviorSubject<Music>(null);
   pmusicSubjects: BehaviorSubject<Music> = new BehaviorSubject<Music>(null);

   musicApi = environment.apiUrl + 'music' ;
   musicRequest: Music;
   intervalLecture: any;
  constructor(private http: HttpClient, private dom: DomSanitizer) {
    this.getRequestMusic();
    this.getRequestMusicById('-1');
    this.ngetRequestMusicById('+1');
  }

  /**
   * Renvoit une UrlSafe selon les données binaires de l'image ou de l'image par defaut
   * @param b64 données binaires de l'image
   * @return SafeUrl
   */
  safeImageUrl(b64: string): SafeUrl{
    if (b64 == null) {
      return this.dom.bypassSecurityTrustUrl('https://static-s.aa-cdn.net/img/ios/1149921086/8bf4d3cd9e6a9bc8a352715f99ac0534')
    } // image par defaut
    return this.dom.bypassSecurityTrustUrl('data:image/png;base64, ' + b64 );
  }

  /**
   * Requete GET pour récupérer la music en train d'être jouée
   * @return any
   */
  getRequestMusic(): any {
    return this.http.get<Music>(this.musicApi, { observe: 'response' })
    .subscribe(
      res => {
        this.musicSubjects.next(res.body);
      });
  }

  /**
   * Requete GET pour récupérer la music précédente selon un id
   * @param id id de la music
   * @return any
   */
  getRequestMusicById(id: string): any {
    const URL = this.musicApi + '/' + id;
    return this.http.get<Music>(URL, { observe: 'response' })
    .subscribe(
      res => {
        this.pmusicSubjects.next(res.body);
      });
  }

  /**
   * Requete GET pour récupérer la music suivante selon un id
   * @param id id de la music
   * @return any
   */
  ngetRequestMusicById(id: string): any {
    const URL = this.musicApi + '/' + id;
    return this.http.get<Music>(URL, { observe: 'response' })
    .subscribe(
      res => {
        this.nmusicSubjects.next(res.body);
      });
  }

  /**
   * Permet de renvoyer l'observable de la music précédente
   * @return Observable<Music>
   */
  getPreviousMusic(): Observable<Music>{
    return this.pmusicSubjects.asObservable();
  }

  /**
   * Permet de renvoyer l'observable de la music actuel
   * @return Observable<Music>
   */
  getActualMusic(): Observable<Music>{
    return this.musicSubjects.asObservable();
  }

  /**
   * Permet de renvoyer l'observable de la music suivante
   * @return Observable<Music>
   */
  getNextMusic(): Observable<Music>{
    return this.nmusicSubjects.asObservable();
  }

  /**
   * Permet d'update les musics affichées sur le lecteur
   * @return void
   */
  updateMusic(): void{
    this.getRequestMusic();
    this.getRequestMusicById('-1');
    this.ngetRequestMusicById('+1');
  }

  /**
   * Permet d'effectuer une requete GET dans le but de récupérer le timestamp actuel
   * @return void
   */
  getCurrentTimeStamp(): void{
    const currentvalue = this.musicSubjects.getValue();
    let newid = false;
    this.http.get<any>(this.musicApi + '/timestamp')
    .subscribe(
      data => {
        currentvalue.timestamp = data.timestamp;
        if (data.id !== currentvalue.id){
          this.updateMusic();
          newid = true;
        }
      }
    );

    if (!newid){
      this.musicSubjects.next(currentvalue);
    }
  }


  /**
   * fonction updateTimeStamp permet de update en temp réel le temp de lecture de la music qui est en train d'être jouée
   * @param action action à effectuer (play/pause)
   * @return void
   */
 updateTimeStamp(action: string): void {
  if (action === 'play') {
    if (this.intervalLecture !== 0) {
      clearInterval(this.intervalLecture);
      this.intervalLecture = 0;
    }
    this.intervalLecture = setInterval(() => { this.getCurrentTimeStamp(); }, 1000);
  }
  else {
    clearInterval(this.intervalLecture);
    this.intervalLecture = 0;
  }
}
}
