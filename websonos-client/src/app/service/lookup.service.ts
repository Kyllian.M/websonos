import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Lookup } from '../interface/Lookup';
import { Music } from '../interface/Music';

@Injectable({
  providedIn: 'root'
})
export class LookupService {
  lookupApi = environment.apiUrl + 'lookup' ;
  lookup: Lookup = {
    limit: 0,
    title: null
  };
  foundMusic: BehaviorSubject<Array<Music>> = new BehaviorSubject<Array<Music>>([]);
  constructor(private http: HttpClient) { }

  /**
   * Permet d'envoyer une requete POST pour récupérer une liste de Music selon un intitulé de recherche
   * @param title intitulé de la recherche
   * @param limit nombre limite de résultat
   * @return void
   */
  sendLookup(title: string, limit: number): void{
    this.lookup.title = title;
    this.lookup.limit = limit;
    // Si un texte est entré on envoit la requête sinon on remet à vide la liste de résultat
    if (title !== ''){
      this.http.post(this.lookupApi, this.lookup)
      .subscribe((data: Array<Music>) => this.foundMusic.next(data),
      error => {
        console.error('There was an error at lookup!', error);
      });
    }
    else{
      this.foundMusic.next([]);
    }

  }

  /**
   * Permet de renvoyer l'observable des music trouvées
   * @return Observable<Array<Music>>
   */
  getMusicFounded(): Observable<Array<Music>>{
    return this.foundMusic.asObservable();
  }

  /**
   * Permet de récupérer l'id de la music selon son titre
   * @param value titre de la music
   * @return any
   */
  getIdByTitle(value: string): any {
    const currentValue = this.foundMusic.getValue();
    const music = currentValue.find(item => item.metadata.title.toUpperCase() === value.toUpperCase().trimEnd());
    if (music != null){
      return music.id;
    }
    else{
      return -1;
    }


  }
}
