import { MusicService } from './music.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Music } from '../interface/Music';


describe('CustomHttpService', () => {
  let service: MusicService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MusicService]
    });

    // inject the service
    service = TestBed.inject(MusicService);
    httpMock = TestBed.inject(HttpTestingController);
  });
  it('should return type of Music', () => {
    service.getActualMusic().subscribe((data: Music) => {
      expect(data).toBeDefined();
    });
  });

  const musicExpect: Music = {
    id: 15,
    playing: false,
    url : '',
    volume: 0,
    metadata: {
      album: '',
      artist: 'Chris Christodoulou',
      composer: null,
      duration: '712',
      filename: 'The Raindrop that Fell to the Sky.mp3',
      genre: '',
      img_b64: '' ,
      title : 'The Raindrop that Fell to the Sky',
    },
    timestamp: 0
  };
  it('should return Music by id', () => {
    service.getNextMusic().subscribe((data: Music) =>
    expect(data).toBeDefined());
  });
});
