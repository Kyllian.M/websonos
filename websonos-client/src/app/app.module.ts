import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatSliderModule } from '@angular/material/slider';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbAlertModule, NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ProgressBarComponent } from './components/progress-bar.component';
import { TripleimageComponent } from './components/tripleimage.component';
import { ScheduleComponent } from './components/schedule.component';
import { FormScheduleComponent } from './components/form-schedule.component';
import { FormsModule } from '@angular/forms';
import { MusicTileComponent } from './components/music-tile.component';
import { MusicPlayerComponent } from './components/music-player.component';
import { SidenavComponent } from './components/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    ProgressBarComponent,
    TripleimageComponent,
    ScheduleComponent,
    FormScheduleComponent,
    MusicTileComponent,
    MusicPlayerComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSliderModule,
    MatProgressBarModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
