import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Schedule } from '../interface/Schedule';
import { ScheduleService } from '../service/schedule.service';

@Component({
  selector: 'app-schedule',
  template: `
  <nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand" routerLink="/"> ↩️Back</a>
  </nav>
  <div class="container">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th>&nbsp;</th>
          <th scope="col">Lundi</th>
          <th scope="col">Mardi</th>
          <th scope="col">Mercredi</th>
          <th scope="col">Jeudi</th>
          <th scope="col">Vendredi</th>
          <th scope="col">Samedi</th>
          <th scope="col">Dimanche</th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor = "let i of hoursSchedule">
          <th scope="row">{{i}}h</th>
          <td *ngFor = "let days of (Schedule|async)?.days" >

          <div *ngFor="let event of days.start_list, let index = index" [ngStyle]="{'background-color':'green'}">
          <span *ngIf="event.time.hour == i">Volume {{event.volume}}</span>
          </div>
          <div *ngFor="let event of days.stop_list, let index = index" [ngStyle]="{'background-color':'red'}">
          <span *ngIf="event.time.hour == i">Stop Music</span>
          </div>
          </td>

        </tr>

    </tbody>
  </table>
  <div *ngIf =" Schedule | async as schedule">
  <app-form-schedule [schedule]="Schedule | async" (newSchedule)="updateSchedule($event)"></app-form-schedule>
  </div>
</div>
  `,
  styles: [`
    .table .thead-dark th {
      text-align:center;
    }
  `]
})
export class ScheduleComponent implements OnInit {

  hoursSchedule: Array<number>;
  Schedule: Observable<Schedule>;
  constructor( private scheduleService: ScheduleService) {

    this.hoursSchedule = Array(24).fill(1).map((x, i) => i );
   }

  ngOnInit(): void {
    this.Schedule = this.scheduleService.getSchedule();
  }
  /**
   * Permet d'update le planning
   * @param schedule nouveau planning
   * @return void
   */
  updateSchedule(schedule: Schedule): void{
    this.scheduleService.postRequestSchedule(schedule);
  }



}
