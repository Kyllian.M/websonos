import { Component, Input, OnInit } from '@angular/core';
import { Music } from '../interface/Music';
import { MusicService } from '../service/music.service';
import { RequestService } from '../service/request.service';

@Component({
  selector: 'app-music-tile',
  template: `
    <div class='container2 divcontain sel{{sel_id == music.id}}'>
      <div>
        <img [src]="musicService.safeImageUrl(music.metadata.img_b64)" class='iconDetails'>
      </div>
      <div style='margin-left:80px;'>
        <h4>{{music.metadata.title}}</h4>
        <div style="font-size:.6em;float:left;">{{music.metadata.artist}}</div>
        <div style="float:right;font-size:.6em">{{music.metadata.album}}</div>
        <br>
        <button style="font-size:.6em;float:left;" class="btn btn-primary"(click)="playmusic()">▶️</button>
      </div>
    </div>
  `,
  styles: [
  `
    .divcontain{
      padding: 5px;
      background-color: rgba(0,0,0,0.5);
      color: white;
      overflow : hidden;
    }

    .seltrue{
      background-color : rgba(127,127,127,0.5);
    }

    img{
      width:  70px;
      height: 70px;
      object-fit:cover;
    }

    .iconDetails {
      float:left;
    }

    .container2 {
      width:100%;
      height:80px;
    }

    h4 {
      margin:0px;
    }
  `
  ]
})
export class MusicTileComponent implements OnInit {

  @Input() music: Music;
  @Input() sel_id: number;

  constructor(
    public musicService: MusicService,
    public requestService: RequestService
  ) { }
  ngOnInit(): void {}

  /**
   * Permet de jouer la musique dans le menu de la playlist
   */
  playmusic(): void{
    this.requestService.sendRequest('play', null, this.music.id, 0);
    setTimeout(() => {this.musicService.updateMusic(); }, 200);
    this.musicService.updateTimeStamp('play');
  }

}
