import { Component, Input,Output, OnInit,EventEmitter } from '@angular/core';
import { RequestService } from '../service/request.service';

@Component({
  selector: 'app-progress-bar',
  template: `<section>
  <div class="row">
  <div class="col-sm-11">
  <mat-slider class="mypg" (mousedown)="toggleEventTime()"(change)="changeTimeStamp($event.value)" thumbLabel min="0" max="{{duration}}" step="1" value="{{timestamp}}" ></mat-slider>
  </div>
  <div class="col-sm-1">
  <br><p>{{remainsTime}}</p>
</div>
  </div>
</section>

  `,
 styles: [
   `
    ::ng-deep .mypg.mat-slider-horizontal .mat-slider-track-background, ::ng-deep .mypg.mat-slider-horizontal .mat-slider-track-wrapper
  , ::ng-deep .mypg.mat-slider-horizontal .mat-slider-track-fill {
      height:20px;

  }

  ::ng-deep .mypg.mat-slider-horizontal{
    min-width: 103%;
  }
   `
 ]
})
export class ProgressBarComponent implements OnInit {

  @Input() timestamp: number;
  @Input() duration: number;
  @Input() remainsTime: string;
  @Output() changeTimeEvent = new EventEmitter<boolean>();

  changeTime = false;

  constructor(private requestService: RequestService) {}
  ngOnInit(): void {}

  /**
   * Permet de changer le timestamp de la music
   * @param newtimestamp nouveau timestamp
   * @return void
   */
  changeTimeStamp(newtimestamp: number): void{
    this.changeTime = false;
    this.requestService.sendRequest(null, null, null, newtimestamp);
    this.changeTimeEvent.emit(this.changeTime);

  }

  /**
   * Permet de savoir quand l'utilisateur change le timestamp de la progressbar
   * @return void
   */
  toggleEventTime(): void{
    this.changeTime = true;
    this.changeTimeEvent.emit(this.changeTime);

  }
}
