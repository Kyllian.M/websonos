import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tripleimage',
  template: `
    <div>
      <img class="imgsec" [src]="src1"/>
      <img class="imgmain" [src]="src2"/>
      <img class="imgsec" [src]="src3"/>
    </div>
  `,
  styles: [
    `
    div{
      width : 100%;
      text-align : center;
    }

    .imgmain{
        width : 500px;
        height : 500px;
    }

    .imgsec{
      width :  260px;
      height : 260px;
    }

    img{
      padding : 3%;
      object-fit:cover;
    }

    @media only screen and (max-width: 1200px) {
      .imgsec{
        display : none;
      }
    }

    @media only screen and (max-width: 500px) {
      .imgmain{
        width : 300px;
        height : 300px;
    }
    }
    `
  ]
})
export class TripleimageComponent implements OnInit {

  @Input() src1: string;
  @Input() src2: string;
  @Input() src3: string;

  constructor() { }

  ngOnInit(): void {
  }

}
