import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import { Schedule } from '../interface/Schedule';
import { ScheduleData } from '../interface/ScheduleData';

@Component({
  selector: 'app-form-schedule',
  template: `
    <form #formElement="ngForm" (ngSubmit)="modifySchedule(formElement)">
      <div class="form-group">
        <div class="col-sm-5">
        <label>Jour</label>
        <select class="custom-select" name="selectedDay" [(ngModel)]="selectedDay">
        <option [ngValue]="0" >Lundi</option>
        <option [ngValue]="1">Mardi</option>
        <option [ngValue]="2">Mercredi</option>
        <option [ngValue]="3">Jeudi</option>
        <option [ngValue]="4">Vendredi</option>
        <option [ngValue]="5">Samedi</option>
        <option [ngValue]="6">Dimanche</option>
        </select>
        </div>
      </div>
      <div class="row">
      <div class="col-sm-6">
      <div class="form-group " *ngFor="let event of schedule.days[selectedDay].start_list,let i=index">
      <label> heure début : </label>
      <input type="number" min="0" max ="23" name="hourStart{{i}}" [(ngModel)]="event.time.hour">
      <label> Minute début : </label>
      <input type="number" min="0" max ="59" name="minuteStart{{i}}" [(ngModel)]="event.time.minute">
      <label> Volume : </label>
      <input type="number" min="0" max ="100" name="minuteStart{{i}}" [(ngModel)]="event.volume">
      <input type="button" (click)="deleteStartPoint(selectedDay,event)" value="❌">
      </div>
      </div>
      <div class="col-sm-6">
      <div class="form-group" *ngFor="let event of schedule.days[selectedDay].stop_list,let i=index">
      <label> heure fin : </label>
      <input type="number" min="0" max ="23" name="hourEnd{{i}}" [(ngModel)]="event.time.hour">
      <label> Minute fin : </label>
      <input type="number" min="0" max ="59" name="minuteEnd{{i}}" [(ngModel)]="event.time.minute">
      <input type="button" (click)="deleteStopPoint(selectedDay,event)" value="❌">
      </div>
      </div>

      </div>
      <div class="row">
      <div class="col-sm-6">
      <input type="button" (click)="addStartPoint(selectedDay)" value="Nouveau point de départ">
      </div>
      <div class="col-sm-6">
      <input type="button" (click)="addStopPoint(selectedDay)" value="Nouveau point d'arrêt">
      </div>
      </div>

      <div class="row my-2">
      <input type="submit" value="Sauvegarder">
      </div>
    </form>
  `,
  styles: [
  ]
})
export class FormScheduleComponent implements OnInit {

  @Input() schedule: Schedule;
  @Output() newSchedule = new EventEmitter<Schedule>();
  selectedDay = 0;
  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Permet d'ajouter un nouveau point de démarrage de la music selon un jour
   * @param indexDay index du jour
   * @return void
   */
  addStartPoint(indexDay: number): void{
    const dataSchedule: ScheduleData = {
      time : {
         hour : 0,
         minute : 0
      },
      volume : 0
    };
    this.schedule.days[indexDay].start_list.push(dataSchedule);
  }

  /**
   * Permet d'ajouter un nouveau point d'arrêt de la music selon un jour
   * @param indexDay index du jour
   * @return void
   */
  addStopPoint(indexDay: number): void{
    const dataSchedule: ScheduleData = {
      time : {
         hour : 0,
         minute : 0
      },
      volume : -1
    };
    this.schedule.days[indexDay].stop_list.push(dataSchedule);
  }

  /**
   * Permet de supprimer un point de démarrage de la music existant selon un jour
   * @param indexDay index du jour
   * @param startPoint point de démarrage à supprimer
   * @return void
   */
  deleteStartPoint(indexDay: number, startPoint: ScheduleData): void{
    const index = this.schedule.days[indexDay].start_list.indexOf(startPoint);
    if (index > -1){
      this.schedule.days[indexDay].start_list.splice(index, 1);
    }
  }

  /**
   * Permet de supprimer un point d'arrêt de la music existant selon un jour
   * @param indexDay index du jour
   * @param stopPoint point d'arrêt à supprimer
   * @return void
   */
  deleteStopPoint(indexDay: number, stopPoint: ScheduleData): void{
    const index = this.schedule.days[indexDay].stop_list.indexOf(stopPoint);
    if (index > -1){
      this.schedule.days[indexDay].stop_list.splice(index, 1);
    }
  }
  /**
   * Permet de sauvegarder la modification du planning
   * @param formElement formulaire
   * @return void
   */
  modifySchedule(formElement: NgForm): void {
    if (formElement.valid) {
      this.newSchedule.emit(this.schedule);
    }
  }



}
