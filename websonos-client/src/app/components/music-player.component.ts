import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Music } from '../interface/Music';
import { Playlist } from '../interface/Playlist';
import { ProgressBarComponent } from './progress-bar.component';
import { LookupService } from '../service/lookup.service';
import { MusicService } from '../service/music.service';
import { PlaylistService } from '../service/playlist.service';
import { RequestService } from '../service/request.service';

@Component({
  selector: 'app-music-player',
  template: `
    <div class="topnav" *ngIf="musicActual | async as music">
      <div class="musicsearch">
        <i class="fas fa-search" aria-hidden="true"></i>
        <input #inputsearch type="text" class="form-control searchbar" (input)="sendLookup($event.target.value,3)"(keyup.enter)="EnterSearch(music.id,music.playing)" placeholder="search music library">
        <ul class="searchbar searchresults">
          <div *ngFor="let result of foundMusics | async">
            <li *ngIf="displayMusicList && inputSearch.value != ''" class="list-group-item" (click)="fillInputLookup($event,result.id,music.id,music.playing)">{{result.metadata.title}} | {{result.metadata.artist}}</li>
          </div>
        </ul>
      </div>
      <label class="title">Websonos</label>
      <span (click)="PlaylistService.getRequestPlaylist(0,1000);navopen=true" class="hamburger">&#9776;</span>
    </div>
    <img class="backgroundcomp" *ngIf="musicActual|async" [src]='musicService.safeImageUrl( (musicActual|async).metadata.img_b64 )'/>
    <div class="container">
      <div *ngIf="musicActual | async as music">
        <app-tripleimage *ngIf="(musicPrevious | async) && (musicNext | async)" [src1]='musicService.safeImageUrl((musicPrevious|async).metadata.img_b64)' [src2]='musicService.safeImageUrl(music.metadata.img_b64)' [src3]='musicService.safeImageUrl((musicNext|async).metadata.img_b64)' ></app-tripleimage>
        <div class="infomusic" style="text-align : center">
          <p id="title">{{(musicActual|async)?.metadata.title}}</p>
          <p>{{(musicActual|async)?.metadata.artist}}</p>
          <button class="btn btn-primary"(click)="previous()">Previous</button>
          <button #btnplaypause class="btn btn-primary"(click)="music.playing ? pause(music.id,music.volume) :play(music.id,music.volume) " >{{music.playing ? 'Pause' : 'Play'}}</button>
          <button class="btn btn-primary"(click)="skip()">Skip</button>
          <div>
            <span class="d-inline-flex align-middle material-icons">volume_up</span>
            <mat-slider (change)="changeVolume($event.value)" thumbLabel min="1" max="100" step="1" value="{{(musicActual|async)?.volume}}" ></mat-slider>
          </div>
        </div>
        <br>
        <app-progress-bar (changeTimeEvent)="onChangedTime($event)" [duration]=music.metadata.duration [timestamp]=music.timestamp [remainsTime]=calculateRemainTime(music.metadata.duration,music.timestamp)></app-progress-bar>
        <a class="clock" href="/manageMusic"><img width="50" height="50" src="https://cdn0.iconfinder.com/data/icons/feather/96/clock-512.png"/></a>
      </div>
      <app-sidenav [playlist]="playlist|async" [musicActual]="musicActual" [opened]="navopen" (eventclose)="navopen=$event"></app-sidenav>
  `,
  styles: [
    `
      /*Slider volume */
      ::ng-deep .mat-accent .mat-slider-thumb,::ng-deep .mat-accent .mat-slider-thumb-label,::ng-deep .mat-accent .mat-slider-track-fill,::ng-deep .mat-accent .mat-slider-track-fill:hover {
        background-color: var(--main-color);
      }

      /* Progress bar */
      .mat-progress-bar{
        height: 20px;
      }

      ::ng-deep .mat-progress-bar-fill::after {
        background-color: var(--main-color);
      }

      /* Add a black background color to the top navigation */
      .topnav {
        background-color: #333;
        height: 30px;
      }

      /* Style the links inside the navigation bar */
      .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
      }

      /* Change the color of links on hover */
      .topnav a:hover {
        background-color: #ddd;
        color: black;
      }

      /* Add a color to the active/current link */
      .topnav a.active {
        background-color: #4CAF50;
        color: white;
      }

      .searchbar{
        width: 100%;
        padding: 10px;
        height: 27px;
      }

      .musicsearch{
        float:right;
        width: 25%;
      }

      .backgroundcomp{
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: -1;

        filter:         blur(100px);
        -webkit-filter: blur(100px);

        background-repeat:repeat-y;
      }

      .searchresults{
        position: absolute;
      }

      .hamburger{
        color: white;
        position: absolute;
        left: 40px;
        top: 5px;
        font-size:30px;
        cursor:pointer;
      }

      .title{
        position: absolute;
        left: 50%;
        top : 6px;
        color: white;
      }

      .infomusic p{
        color: white;
        font-size: 15px;
        text-shadow: 0 0 2px black, 0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black,0 0 2px black;
      }

      .infomusic #title{
        font-size: 30px;
        font-weight: bold;
      }

      .clock{
        position: fixed;
        bottom: 5px;
        right:5px;
      }
    `
  ]

})
export class MusicPlayerComponent implements OnInit, AfterViewInit {

  constructor(
    public musicService: MusicService,
    private requestService: RequestService,
    private lookupService: LookupService,
    public PlaylistService: PlaylistService,
    private dom: DomSanitizer,
    private router: Router)
  {
    document.documentElement.style.setProperty('--main-color', '#4b40c8');
  }

  title = 'websonos-client';
  musicActual: Observable<Music>;
  musicPrevious: Observable<Music>;
  musicNext: Observable<Music>;

  playlist: Observable<Playlist>;
  foundMusics: Observable<Array<Music>>;
  displayMusicList: boolean = false;
  navopen: boolean = false;
  intervalLecture: any;
  remainsTime: string;

  @ViewChild('btnplaypause', {static: false}) myBtn: HTMLButtonElement;
  @ViewChild('inputsearch', {static: false}) inputSearch: HTMLInputElement;
  @ViewChild('bar', {static: false}) timebar: ProgressBarComponent;

  ngOnInit(): void {
    this.musicActual = this.musicService.getActualMusic();
    this.musicPrevious = this.musicService.getPreviousMusic();
    this.musicNext = this.musicService.getNextMusic();
    this.foundMusics = this.lookupService.getMusicFounded();
    this.playlist = this.PlaylistService.getPlaylist();
  }

  ngAfterViewInit(): void{
    setTimeout(() => {
      if (this.myBtn.innerHTML === 'Pause'){
        this.updateTimeStamp('play');
      }
    }, 400);
    this.PlaylistService.getRequestPlaylist('0', '1000');
  }

  // EVENT
  /**
   * fonction play pour lancer la music lors du click sur le bouton
   * @param id de la music et son temp écoulé actuel
   * @return void
   */
  play(idMusic: number, volume: number, timestamp: number = null): void {
    this.requestService.sendRequest('play', volume, idMusic, timestamp);
    setTimeout(() => {this.musicService.updateMusic(); }, 100); // permet d'update la variable boolean playing venant du serveur
    this.updateTimeStamp('play');
  }

  /**
   * fonction pause pour mettre en pause lors du click sur le bouton
   * @param id de la music et son temp écoulé actuel
   * @return void
   */
  pause(idMusic: number, volume: number, timestamp: number = null): void {
    this.requestService.sendRequest('pause', volume, idMusic, timestamp);
    setTimeout(() => { this.musicService.updateMusic(); }, 100); // permet d'update la variable boolean playing venant du serveur
    this.musicService.updateTimeStamp('pause');
  }

  /**
   * fonction skip permettant de passer à la music suivante
   * @return void
   */
  skip(): void {
    this.requestService.sendRequest('skip');
    if (this.intervalLecture === 0){
    this.updateTimeStamp('play');
  }
  }

  /**
   * fonction previous permettant de revenir à la music précédente
   * @return void
   */
  previous(): void {
    this.requestService.sendRequest('previous');
    if (this.intervalLecture === 0){
      this.updateTimeStamp('play');
    }
  }

  /*
  * fonction updateTimeStamp permet de update en temp réel le temp de lecture de la music qui est en train d'être jouée
  * @param action à éffectué (play ou pause)
  * @return void
  */
  updateTimeStamp(action: string): void{
    if (action === 'play' ){
      if (this.intervalLecture !== 0){
        clearInterval(this.intervalLecture);
        this.intervalLecture = 0;
      }
      this.intervalLecture = setInterval(() => { this.musicService.getCurrentTimeStamp(); }, 1000);
    }
    else{
      clearInterval(this.intervalLecture);
      this.intervalLecture = 0;
    }
  }

  /**
   * fonction changeVolume permet de update le volume en temps réel de la musique en cours de lecture
   * @param value volume à setter
   * @return void
   */
  changeVolume(value: number): void{
    this.requestService.sendRequest('volume', value);
    setTimeout(() => {this.musicService.updateMusic(); }, 100);
  }

  /**
   * fonction sendLookup permet d'envoyer une requête de recherche de musique lors du remplissage de l'input de recherche
   * @param title: intitulé de la recherche
   * @param limit: limite du nombre de résultat
   * @return void
   */
  sendLookup(title: string, limit: number): void{
    this.displayMusicList = true;
    this.lookupService.sendLookup(title, limit);
  }

  /**
   * fonction fillInputLookup permet de remplir l'input lors du click sur un choix de music
   * @param event: event du click de l'input
   * @param idMusic: id de la music choisis
   * @param idMusicActuel: id de la music en train d'etre jouée
   * @param isplaying: true si une music est en train d'être jouer sinon false
   * @return void
  */
  fillInputLookup(event: any, idMusic: number, idMusicActuel: number, isplaying: boolean): void{
    const music = event.path[0].innerHTML.split('|');
    this.inputSearch.value = music[0];
    this.displayMusicList = false;
    this.TooglePlayPause(idMusic, idMusicActuel, isplaying);
  }

  /**
   * fonction EnterSearch permet de gérer l'appuie sur le bouton entrée dans l'input
   * @param idMusicActuel: id de la music en train d'etre jouée
   * @param isplaying: true si une music est en train d'être jouer sinon false
   * @return void
   */
  EnterSearch(idMusicActuel: number, isplaying: boolean): void{
    const id = this.lookupService.getIdByTitle(this.inputSearch.value);
    this.TooglePlayPause(id, idMusicActuel, isplaying);
  }

  /**
   * fonction TooglePlayPause permet de gérer l'appuie sur le bouton entrée dans l'input
   * @param idMusic: id de la music choisis
   * @param idMusicActuel: id de la music en train d'etre jouée
   * @param isplaying: true si une music est en train d'être jouer sinon false
   * @return void
   */
  TooglePlayPause(idMusic: number, idMusicActuel: number , isplaying: boolean): void{
    if (isplaying){
      this.pause(idMusicActuel, null , 0);
    }
    this.play(idMusic, null , 0);
    this.inputSearch.value = ''; // Pour éviter le Spam
  }

  /**
   * fonction calculateRemainTime permet de calculer le temps restant avant la fin de la music
   * @param duration durée total de la music
   * @param timestamp temp actuel de lecture
   * @return String : le temps restant parsé
   */
  calculateRemainTime(duration: number, timestamp: number): string{
    let remains = duration - timestamp;
    let m = 0;
    if (remains >= 60){
      m = Math.floor(remains / 60);
      remains %= 60;
    }
    if (remains >= 0 && remains < 10) {
      return this.remainsTime = '- ' + m + ':0' + remains ;
    }
    else{
      return this.remainsTime = '- ' + m + ':' + remains ;
    }
  }

  /**
   * onChangeTime() permet d'arreter ou de relancer l'update du timestamp pendant que l'on change
   * la valeur de la bar de progression
   * @param eventTime boolean vrai si on est en train de changer le timestamp faux sinon
   * @return void
   */
  onChangedTime(eventTime: boolean): void{
    if (eventTime){
      this.updateTimeStamp('pause');
    }
    else{
      this.updateTimeStamp('play');
    }
  }

  /**
   * Permet de naviguer vers la route /manageMusic
   * @return void
   */
  NavGestionSchedule(): void {
    this.router.navigate(['/manageMusic']);
  }

}
