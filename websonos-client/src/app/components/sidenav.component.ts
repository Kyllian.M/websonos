import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Music } from '../interface/Music';
import { Playlist } from '../interface/Playlist';

@Component({
  selector: 'app-sidenav',
  template: `
    <div *ngIf="playlist" #mySidenav class="sidenav open{{opened}}">
      <a href="javascript:void(0)" class="closebtn" (click)="close()">&times;</a>
      <div *ngFor="let music of playlist.music">
        <app-music-tile [music]="music" [sel_id]="musicActual.id"></app-music-tile>
      </div>
    </div>
  `,
  styles: [
    `
    .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      transition: 0.5s;
      padding-top: 60px;
    }

    .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 25px;
      color: #818181;
      display: block;
      transition: 0.3s;
    }

    .sidenav a:hover {
      color: #f1f1f1;
    }

    .sidenav .closebtn {
      position: absolute;
      top: 0;
      right: 25px;
      font-size: 36px;
      margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }

    .openfalse{
      width : 0px;
    }

    .opentrue{
      width : 400px;
    }
    `
  ]
})
export class SidenavComponent implements OnInit {
  @Input() opened : Boolean;
  @Input() playlist : Playlist;
  @Input() musicActual : Music;
  @Output() eventclose = new EventEmitter<Boolean>();

  constructor() {}
  ngOnInit(): void {}

  close(){
    this.eventclose.emit(false);
  }
}
