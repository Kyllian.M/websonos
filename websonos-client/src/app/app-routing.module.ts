import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MusicPlayerComponent } from './components/music-player.component';
import { ScheduleComponent } from './components/schedule.component';

const routes: Routes = [  { path: '', component: MusicPlayerComponent },
  {path: 'manageMusic', component: ScheduleComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
