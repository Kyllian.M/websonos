# Websonos
Ce projet consiste a pouvoir controller un serveur springboot diffusant de la musique en continu depuis un frontend angular esthetique, le serveur et le client communiquent au travers d'APIs Rest

## Features :
- Gestion de la lecture des musiques du serveur
- Controles :
    - Lecture / Pause
    - Precedent / Suivant
    - Controle du volume (0 a 100%)
    - Controle du playback
- affichage du nom de la musique et de son artiste en plus de la pochette d'album
- responsive avec les affichages mobiles
- recherche de musique dans la librairie a l'aide de mots clés
- affichage de la playlist et possibilité de jouer une musique selctionnée
- calendrier hebdomadaire avec demarage / arrets programmés dans le temps a l'aide d'un formulaire

## Project Descriptions & Expectations :
- API can be:
    - Public API: (for instance https://jsonplaceholder.typicode.com/)
    - Private API: your own API that is easily available.
    - [OK] Local server exposing API
- Web Application should use Angular features such as :
    - [OK] Components
    - [OK] Services
    - [OK] Routing
    - [OK] Observables
    - [OK] Testing
- Project should have a decent UI, you can use:
    - [OK] own CSS
    - [OK] third party CSS library (bootstrap, ...)
    - [OK] third party components (Angular Material, NgBootstrap, ...)

## Concept art : 
![main](https://gitlab.com/Kyllian.M/websonos/-/raw/master/ressources/concept1.png)
![sec](https://gitlab.com/Kyllian.M/websonos/-/raw/master/ressources/concept2.png)

## Final Product : 
![main](https://gitlab.com/Kyllian.M/websonos/-/raw/master/ressources/final1.png)
![sec](https://gitlab.com/Kyllian.M/websonos/-/raw/master/ressources/final2.png)
![ter](https://gitlab.com/Kyllian.M/websonos/-/raw/master/ressources/final3.png)

## Installation

Pour procéder à l'installation,

### Serveur :
La facon la plus simple est d'utiliser un environnement de devellopement (IDE) Java afin de pouvoir build avec Maven et Run le projet SpringBoot (Eclipse ou IntelliJ Recommandé) Il faudra egallement installer l'extension "Lombok" si elle n'est pas déja installée sur votre IDE
 (https://www.eclipse.org/downloads/ , https://projectlombok.org/)
 
- Il est recommandé d'avoir ffmpeg installé dans le cas d'une execution sur linux afin d'avoir les images des musiques (non requis)

### Client :
```bash
cd websonos-client
npm install
ng serve
```

## Utilisation
Par defaut le serveur aura pour librairie musicale les echantillons musicaux situés dans le dossier websonos-server2/music mais vous pouvez changer le dossier de librairie musicale dans le fichier settings.json du serveur

Il n'y a plus qu'à se connecter sur http://localhost:4200/

(Pour terminer les processus, appuyer deux fois sur Ctrl-C )

# API Rest :
## Commande Lecteur : 
```json
POST /command  
{  
    id_music: 1 // id de la music //nullable  
    status : "play"; // possible value (play,pause,skip,previous,replay)  
    timestamp: 134; // nb en seconde //nullable  
}  
Response: HTTP OK
```
## Commande Lookup :
```json
POST /lookup  
{  
    lookup:{  
        title:  
        limit:  
    }  
}  
Response:   
{  
    music[];  
}  
```

## Commande get music 
```json
GET /music?id=3     //(current music)
Response:
{
    music:{
        music_id:
        titre :
        artiste : 
        genre :
        url_image :
        timestamp:
    }
}
```

## Commande get current timestamp
```json
GET /music/timestamp
Response:
{
    timestamp : 5    //le serveur renvoit le timestamp toute les secondes
}
```

## Commande get listplaylist
```json
GET /playlist
Response:
{
    playlist[]:{
        playlist_id:
        date_create:
        name : 

    }
}
```

## Commande get playlist
```json 
GET /playlist/{:id}?offset=0&limit=30
Response:
{
    playlist:{
        playlist_id:
        name : 
        date_create:
        music[]
    }
}
```


## Curl exemples
```bash
curl -H "Content-type: application/json" -X POST -d '{"id_music":1,"status":"play","timestamp":134}' http://localhost:8080/command
curl -H "Content-type: application/json" -X GET http://localhost:8080/music/play
curl -H "Content-type: application/json" -X GET http://localhost:8080/music
curl -H "Content-type: application/json" -X GET http://localhost:8080/music/timestamp 
	--> retourne le timestamp et id uniquement de la musique lue actuellement
curl -H "Content-type: application/json" -X GET http://localhost:8080/schedule
	--> retourne le planning des allumages et arrets automatiques de la musique
curl -H "Content-type: application/json" -X POST -d '{"title":"Rain","limit":3}' http://localhost:8080/lookup
```
