package com.websonosserver.WebSonosServer.domain;
import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.DataLine.Info;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
public class Music {
	
	public static long NEXT_ID = 0;
	
	/**
	 * Inner-class servant a lancer la lecture de l'extrait audio sur un thread différent
	 */
	private class PlayThread extends Thread {
		public Music s;
		public void run() {
			s.play_single_thread();
		}
	}
	
	long id = -1;
	//fichier loadé
	File file;
	//longueur des données
	int length;
	//lecture arretée ?
	boolean stopped = true;
	//thread séparé de lecture du son
	PlayThread thread = null;
	//playlist contenant la musique
	Playlist parent = null;
	//metadonnées du fichier musical
	MusicMetadata metadata;
	
	float timestamp = 0;
	
	
	
	public Music(String filepath) {
		id = NEXT_ID;
		NEXT_ID++;
		file = new File(filepath);
		metadata = new MusicMetadata();
	}
	
	public void updateMetadata() {
		metadata = new MusicMetadata(file);
	}

	/**
	 * lance la lecture multi-threadée de l'echantillon audio
	 */
	public void play(){
		if(stopped) {
			thread = new PlayThread();
			stopped = false;
			thread.s = this;
			thread.start();
		}
	}

	/**
	 * Fonction de lecture d'un son
	 */
	public void play_single_thread (){
        try (AudioInputStream in = getAudioInputStream(file)) {

            final AudioFormat outFormat = getOutFormat(in.getFormat());
            final Info info = new Info(SourceDataLine.class, outFormat);

            try (final SourceDataLine line =(SourceDataLine) AudioSystem.getLine(info)) {

                if (line != null) {
                    line.open(outFormat);
                    line.start();
                    stream(getAudioInputStream(outFormat, in), line);
                    line.drain();
                    line.stop();
                }
            }

        } catch (Exception e) {
        	System.out.println(file.getAbsolutePath());
            e.printStackTrace();
        }
	}

	/**
	 * arrete la lecture multi-threadée d'un extrait audio
	 */
	public void stop(){
		stopped = true;
		if(thread != null)
			thread.interrupt();
	}
	
	public void reset() {
		timestamp = 0;
	}
	
	public void setPlayback(int seconds) {
		if(!stopped) {
			stop();
			timestamp = seconds;
			while(thread.isAlive()) {}
			play();
		}
		else{
			timestamp = seconds;
		}
	}

    private AudioFormat getOutFormat(AudioFormat inFormat) {
        final int ch = inFormat.getChannels();

        final float rate = inFormat.getSampleRate();
        return new AudioFormat(PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
    }

    private void stream(AudioInputStream in, SourceDataLine line) 
        throws IOException {
        final byte[] buffer = new byte[4096];
        int cpt = (int)(timestamp*(44100*4));
        in.skip((long) (timestamp*44100/2));
        for (int n = 0; n != -1 && !stopped; n = in.read(buffer, 0, buffer.length)) {
            line.write(buffer, 0, n);
            cpt+=buffer.length;
            if(!stopped) {
            	timestamp = (float) (cpt*1./(44100*4));
            }
        }
        if(!stopped && parent != null && parent.isAutoplay())
        	parent.next();
        stopped = true;
    }
}
