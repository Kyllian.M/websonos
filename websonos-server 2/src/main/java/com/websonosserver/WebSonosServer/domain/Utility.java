package com.websonosserver.WebSonosServer.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.*;

public class Utility {
	
	public static String getTimeDifference (LocalDateTime fromDateTime,LocalDateTime toDateTime) {
		LocalDateTime tempDateTime = LocalDateTime.from( fromDateTime );

		long years = tempDateTime.until( toDateTime, ChronoUnit.YEARS );
		tempDateTime = tempDateTime.plusYears( years );

		long months = tempDateTime.until( toDateTime, ChronoUnit.MONTHS );
		tempDateTime = tempDateTime.plusMonths( months );

		long days = tempDateTime.until( toDateTime, ChronoUnit.DAYS );
		tempDateTime = tempDateTime.plusDays( days );

		long hours = tempDateTime.until( toDateTime, ChronoUnit.HOURS );
		tempDateTime = tempDateTime.plusHours( hours );

		long minutes = tempDateTime.until( toDateTime, ChronoUnit.MINUTES );
		tempDateTime = tempDateTime.plusMinutes( minutes );

		long seconds = tempDateTime.until( toDateTime, ChronoUnit.SECONDS );
		
		if(years == 0 && months == 0 && days==0 && hours == 0 && minutes == 0 && seconds == 0)
			return "less than a second";
		
		return ( 
			(years == 0 ? "" : years + " years ") + 
			(months == 0 ? "" : months + " months ") + 
	        (days == 0 ? "" : days + " days ") +
	        (hours == 0 ? "" : hours + " hours ") +
	        (minutes == 0 ? "" : minutes + " minutes ") +
	        (seconds == 0 ? "" : seconds + " seconds."));
	}
	
	//Donne la liste des noms des fichiers se trouvant dans un dossier selon l'extension demandée
	public static List<String> getListFile(String pathDir,String extension){
		File myDir = new File(pathDir);
	 	List<String> listmap=new ArrayList<String>();
	 	for(String str: myDir.list()) {
	 		if(str.contains("."+extension)) {
	 			listmap.add(str);
	 		}
	 	}
       for (String string : listmap) {
    	   System.out.println(string);
       }
       
       return listmap;

	}
	
	//Donne la liste des noms des fichiers se trouvant dans un dossier
	public static List<String> getListFile(String pathDir){
		File myDir = new File(pathDir);
	 	List<String> listmap=new ArrayList<String>();
	 	for(String str: myDir.list()) {
	 		listmap.add(str);
	 	}
       for (String string : listmap) {
    	   System.out.println(string);
       }
       
       return listmap;

	}
	
	public static String getTextFromFile(String path) {
		String text = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    text = sb.toString();
		    br.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			text = e.getMessage();
		}
		return text;
	}
	
	public static JSONObject getJsonFromFile(String path) {
		try {
			return new JSONObject(getTextFromFile(path));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<String> splitOrReturn(String input,String regex){
		List<String> ret = null;
		String[] split = input.split(regex);
		if(split.length > 0)
			ret = Arrays.asList(split);
		else {
			ret = new ArrayList<>();
			ret.add(input);
		}
		return ret;
	}
}
