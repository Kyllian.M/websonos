package com.websonosserver.WebSonosServer.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@NoArgsConstructor
public class Settings {
	String music_library_path,default_schedule;
	Boolean autoplay;
}
