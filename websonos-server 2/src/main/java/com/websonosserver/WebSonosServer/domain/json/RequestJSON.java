package com.websonosserver.WebSonosServer.domain.json;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
@ToString
public class RequestJSON {
	Integer id_music;
	String status;
	Integer timestamp;
	Integer volume;
}
