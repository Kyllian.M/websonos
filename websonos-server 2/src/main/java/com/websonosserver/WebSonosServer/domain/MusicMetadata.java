package com.websonosserver.WebSonosServer.domain;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.DefaultHandler;
import com.websonosserver.WebSonosServer.WebSonosServerApplication;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString
@FieldDefaults(level=AccessLevel.PRIVATE)
public class MusicMetadata {
	String filename,title,artist,composer,genre,album,duration;
	@ToString.Exclude String img_b64;
	
	MusicMetadata(File music_file){
		try {
            InputStream input = new FileInputStream(music_file);
            ContentHandler handler = new DefaultHandler();
            Metadata metadata = new Metadata();
            Parser parser = new Mp3Parser();
            ParseContext parseCtx = new ParseContext();
            parser.parse(input, handler, metadata, parseCtx);
            input.close();
            
            // Retrieve the necessary info from metadata
            // Names - title, xmpDM:artist etc. - mentioned below may differ based
            filename = music_file.getName();
            title = metadata.get("title");
            artist = metadata.get("xmpDM:artist");
            composer = metadata.get("xmpDM:composer");
            genre = metadata.get("xmpDM:genre");
            album = metadata.get("xmpDM:album");
            duration = "" + ( Math.round(Float.parseFloat(metadata.get("xmpDM:duration"))/1000) );
            
            img_b64 = getImage(music_file);
            //System.out.println(img_b64);
            resolveNonStandard(music_file);
            System.out.println(this);
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	MusicMetadata(){
		filename = title = artist = composer = genre = album = "Server startup ... Loading...";
		duration = "60";
		img_b64 = null;
	}
	
	String getImage(File input) {
		String executable = "./ffmpeg/bin/ffmpeg.exe";
		if(SystemUtils.IS_OS_LINUX)
			executable = "/usr/bin/ffmpeg";
		String out = "images/" + input.getName() + ".png";
		String[] line = {executable, "-i" , input.getAbsolutePath(), out};
        try {
			Process proc = Runtime.getRuntime().exec(line);
			proc.waitFor(1,TimeUnit.SECONDS);
			File outfile = new File(out);
			byte[] fileContent = FileUtils.readFileToByteArray(outfile);
			outfile.delete();
			return Base64.getEncoder().encodeToString(fileContent);
			
		} catch (Exception e) {
			System.out.println("no image found for file " + input.getName());
			return null;
		}
	}
	
	private void resolveNonStandard(File origin) {
		boolean title_changed = false;
		
		if(propertyIsNull(artist)) {
			String title_scrap = Utility.splitOrReturn(filename, "\\.").get(0);
			ArrayList<String> separate_datas = separateTitleArtistDatas(title_scrap);
			if(separate_datas.size() > 1) {
				artist = separate_datas.get(0);
				title = separate_datas.get(1);
				title_changed = true;
			}
			else {
				separate_datas = separateTitleArtistDatas(title);
				if(separate_datas.size() > 1) {
					artist = separate_datas.get(0);
					title = title_scrap;
					title_changed = true;
				}
			}
		}
		
		if(!title_changed) {
			title = Utility.splitOrReturn(filename, "\\.").get(0);//
			ArrayList<String> separate_datas = separateTitleArtistDatas(title);
			if(separate_datas.size() > 1) {
				title = separate_datas.get(1);
			}
		}
		
		if(propertyIsNull(genre)) {
			ArrayList<String> genres = WebSonosServerApplication.MUSIC_GENRE_LIBRARY.match(this);
			genre = "";
			for(int i=0;i<genres.size();i++) {
				genre += genres.get(i);
				if(i != genres.size() - 1)
					genre+=",";
			}
		}
		
		title = title.replaceAll("(\\[|\\(|\\|)[A-Za-z]+(\\]|\\)|\\|)", "");
		
		if(propertyIsNull(album)) {
			album = origin.getParentFile().getName();
		}
	}
	
	private ArrayList<String> separateTitleArtistDatas (String data){
		ArrayList<String> datas = new ArrayList<>();
		
		List<String> samples = Utility.splitOrReturn(data, " - ");
		for(String sample : samples) {
			List<String> samples2 = Utility.splitOrReturn(sample, " \\| ");
			for(String sample2 : samples2) {
				datas.add(sample2);
			}
		}
		
		return datas;
	}
	
	private boolean propertyIsNull(String property) {
		if(property == null)
			return true;
		String[] invalid_values = {"null","","other","Other","Null"};
		boolean ret = false;
		for(String value : invalid_values) {
			ret = ret || property.equals(value);
			if(ret)
				return true;
		}
		return false;
	}
}
