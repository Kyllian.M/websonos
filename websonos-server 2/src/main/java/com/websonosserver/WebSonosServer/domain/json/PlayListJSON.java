package com.websonosserver.WebSonosServer.domain.json;

import java.util.ArrayList;
import java.util.Date;
import com.websonosserver.WebSonosServer.domain.Playlist;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
public class PlayListJSON {
	long id;
	String name;
	Date date_create;
	ArrayList<MusicJSON> music = new ArrayList<>();
	
	public PlayListJSON(Playlist playlist) {
		this(playlist,-1,-1);
	}
	
	public PlayListJSON(Playlist playlist,int offset,int limit) {
		id = playlist.getId();
		name = playlist.getName();
		date_create = playlist.getDate_create();
		
		int off = offset == -1 ? 0 : offset;
		for(int i=off;i<playlist.getMusics().size() && (limit == -1 || i-off < limit);i++)
			music.add(new MusicJSON(playlist.getMusics().get(i)));
	}
}
