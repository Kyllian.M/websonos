package com.websonosserver.WebSonosServer.domain.json;
import com.websonosserver.WebSonosServer.domain.Music;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString
@FieldDefaults(level=AccessLevel.PRIVATE)
public class TimestampJSON {
	int timestamp;
	long id;
	
	public TimestampJSON(Music music) {
		id = music.getId();
		timestamp = (int)music.getTimestamp();
	}
}
