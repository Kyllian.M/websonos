package com.websonosserver.WebSonosServer.domain;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.poi.hslf.record.Sound;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.websonosserver.WebSonosServer.WebSonosServerApplication;
import com.websonosserver.WebSonosServer.domain.json.MusicJSON;
import com.websonosserver.WebSonosServer.domain.json.RequestJSON;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Playlist {
	public static long NEXT_ID = 0;
	
	private long id = -1;
	private Date date_create;
	private boolean autoplay = true;
	private String name;
	private int current_index = 0;
	private ArrayList<Music> musics = new ArrayList<>();
	private MetadataThread metadatathread = null;
	
	private class MetadataThread extends Thread {
		public Playlist s;
		public void run() {
			for(Music music : s.getMusics()) {
				music.updateMetadata();
			}
			System.out.println("finished metadata updates");
		}
	}
	
	public Playlist(String folder_path) {
		id = NEXT_ID;
		NEXT_ID++;
		findAllMP3Recursive(folder_path);
		System.out.println("Research Done, found " + musics.size() + " mp3 files");
		Collections.shuffle(musics);
		date_create = new Date();
		name = "Randomly generated playlist";
	}
	
	public void updateMetadata() {
		metadatathread = new MetadataThread();
		metadatathread.s = this;
		metadatathread.run();
	}
	
	public void findAllMP3Recursive(String folder_path) {
		List<String> filelist = Utility.getListFile(folder_path);
		for(String filename : filelist) {
			File file = new File(folder_path + "/" + filename);
			if(file.isDirectory())
				findAllMP3Recursive(file.getAbsolutePath());
			else
				if(file.getName().endsWith(".mp3")) {
					Music music = new Music(file.getAbsolutePath());
					music.setParent(this);
					musics.add(music);
				}
		}
	}
	
	public void play() {
		System.out.println("Playing : '" + musics.get(current_index).getFile().getName() + "'");
		musics.get(current_index).play();
	}
	
	public void stop() {
		musics.get(current_index).stop();
	}
	
	public void reset() {
		musics.get(current_index).reset();;
	}
	
	public void next() {
		stop();
		if(current_index < musics.size())
			musics.get(current_index).reset();
		current_index++;
		if(current_index >= musics.size())
			current_index = 0;
		play();
	}
	
	public void previous() {
		stop();
		musics.get(current_index).reset();
		current_index--;
		if(current_index < 0)
			current_index = musics.size() - 1;
		play();
	}
	
	public Integer getIndexOfMusic(int id) {
		int cpt = 0;
		for(Music music : getMusics()) {
			if(music.getId() == id)
				return cpt;
			cpt++;
		}
		return null;
	}
	
	public void applyRequest(RequestJSON request) {
		if(request.getStatus() != null) {
			switch(request.getStatus()) {
				case "play" :
					if(request.getId_music() == null)
						play();
					break;
				case "pause" :
					stop();		
					break;
				case "skip" :
					next();
					break;
				case "previous" :
					previous();
					break;
				case "replay" :
					reset();
					break;
				case "delete" :
					int index = request.getId_music() == null ? current_index : getIndexOfMusic(request.getId_music()) ;
					Music music = musics.get(index);
					music.getFile().renameTo(new File("deleted/" + music.getFile().getName()));
					musics.remove(index);
					next();
					break;
			}
		}
		if(request.getTimestamp() != null) {
			musics.get(current_index).setPlayback(request.getTimestamp());
		}
		
		if(request.getId_music() != null && request.getStatus().equals("play")) {
			stop();
			current_index = getIndexOfMusic(request.getId_music());
			play();
		}
		if(request.getVolume() != null)
			WebSonosServerApplication.volumeControl.setVolume(request.getVolume());
	}
}
