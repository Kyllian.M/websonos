package com.websonosserver.WebSonosServer.domain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
public class MusicGenreLibrary {
	public static String LIBRARY_LOCATION = "music_genres.txt";
	List<String> genres = null;
	
	public MusicGenreLibrary() {
		String[] array = Utility.getTextFromFile(LIBRARY_LOCATION).split("\r\n");
		genres = Arrays.asList(array);
	}
	
	public ArrayList<String> match (MusicMetadata data){
		ArrayList<String> matches = new ArrayList<>();
		for(String genre : genres) {
			if(data.getTitle().toLowerCase().contains(genre.toLowerCase()) || data.getFilename().toLowerCase().contains(genre.toLowerCase())) {
				matches.add(genre);
			}
		}
		return matches;
	}
}
