package com.websonosserver.WebSonosServer.domain;
import java.util.ArrayList;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;

public class VolumeControl {
	
	ArrayList<FloatControl> controls = new ArrayList<>();

	public VolumeControl() {
		javax.sound.sampled.Mixer.Info[] mixers = AudioSystem.getMixerInfo();
	    for(int i=0;i<mixers.length;i++){
	        Mixer.Info mixerInfo = mixers[i];
	        Mixer mixer = AudioSystem.getMixer(mixerInfo);
	        Line.Info[] lineinfos = mixer.getTargetLineInfo();
	        for(Line.Info lineinfo : lineinfos){
	            try {
	                Line line = mixer.getLine(lineinfo);
	                line.open();
	                if(line.isControlSupported(FloatControl.Type.VOLUME)){
	                    final FloatControl control = (FloatControl) line.getControl(FloatControl.Type.VOLUME);
	                    controls.add(control);
	                    control.setValue((float) 0.5);
	                    int value = (int) (control.getValue()*100);
	                }
	            } catch (LineUnavailableException e) {}
	        }
	    }
	}
	
	public void setVolume(int prct) {
		for(FloatControl control : controls) {
			control.setValue(1f*prct/100);
		}
	}
	
	public int getVolume() {
		return (int) (controls.get(0).getValue()*100);
	}
	
}