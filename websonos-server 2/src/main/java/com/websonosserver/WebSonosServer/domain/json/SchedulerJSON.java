package com.websonosserver.WebSonosServer.domain.json;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.websonosserver.WebSonosServer.domain.Playlist;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
public class SchedulerJSON {
	
	@AllArgsConstructor
	@NoArgsConstructor
	@EqualsAndHashCode
	@ToString
	private static class TimeHour{
		public int hour,minute;
	}
	@AllArgsConstructor
	@NoArgsConstructor
	private static class DaySchedule {
		@AllArgsConstructor
		@NoArgsConstructor
		public static class TimeVolume{
			public TimeHour time;
			public int volume;
		}
		public ArrayList<TimeVolume> start_list = new ArrayList<>();
		public ArrayList<TimeVolume> stop_list  = new ArrayList<>();
		
		public void addStart(TimeHour time,int volume) {
			start_list.add(new TimeVolume(time, volume));
		}
		
		public void addStop(TimeHour time) {
			stop_list.add(new TimeVolume(time, -1));
		}
	}
	DaySchedule[] days = new DaySchedule[7];
	
	
	public SchedulerJSON() {
		for(int i=0;i<days.length;i++)
			days[i] = new DaySchedule();
	}
	
	public DaySchedule get(String attributes) {
		switch(attributes.toUpperCase()) {
		case "MONDAY":
			return days[0];
		case "TUESDAY":
			return days[1];
		case "WEDNESDAY":
			return days[2];
		case "THURSDAY":
			return days[3];
		case "FRIDAY":
			return days[4];
		case "SATURDAY":
			return days[5];
		case "SUNDAY":
			return days[6];
		}
		return null;
	}
	
	public void activate(Playlist playlist) {
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        
        String day = null;
        switch(cal.get(Calendar.DAY_OF_WEEK)) {
        case Calendar.MONDAY:
        	day = "monday";
        	break;
        case Calendar.TUESDAY:
        	day = "tuesday";
        	break;
        case Calendar.WEDNESDAY:
        	day = "wednesday";
        	break;
        case Calendar.THURSDAY:
        	day = "thursday";
        	break;
        case Calendar.FRIDAY:
        	day = "friday";
        	break;
        case Calendar.SATURDAY:
        	day = "saturday";
        	break;
        case Calendar.SUNDAY:
        	day = "sunday";
        	break;
        }
        
        TimeHour thistime = new TimeHour(cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE));
        
        for(DaySchedule.TimeVolume time : get(day).start_list) {
        	if(time.time.equals(thistime)) {
        		playlist.play();
        		break;
        		//TODO Gérer Volume
        	}
        }
        for(DaySchedule.TimeVolume time : get(day).stop_list) {
        	if(time.time.equals(thistime)) {
        		playlist.stop();
        		break;
        	}
        }
	}
}
