package com.websonosserver.WebSonosServer.domain.json;
import java.io.IOException;

import com.websonosserver.WebSonosServer.WebSonosServerApplication;
import com.websonosserver.WebSonosServer.domain.Music;
import com.websonosserver.WebSonosServer.domain.MusicMetadata;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level=AccessLevel.PRIVATE)
public class MusicJSON {
	MusicMetadata metadata;
	long id;
	String url;
	int timestamp;
	int volume;
	boolean playing;
	
	public MusicJSON(Music music) {
		id = music.getId();
		metadata = music.getMetadata();
		try {
			url = music.getFile().getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		timestamp = (int)music.getTimestamp();
		playing = !music.isStopped();
		
		volume = WebSonosServerApplication.volumeControl.getVolume();
	}
}
