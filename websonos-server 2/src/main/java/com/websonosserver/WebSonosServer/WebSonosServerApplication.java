package com.websonosserver.WebSonosServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.websonosserver.WebSonosServer.domain.MusicGenreLibrary;
import com.websonosserver.WebSonosServer.domain.VolumeControl;


@SpringBootApplication
public class WebSonosServerApplication {
	public static MusicGenreLibrary MUSIC_GENRE_LIBRARY = new MusicGenreLibrary();
	public static VolumeControl volumeControl = new VolumeControl();

	public static void main(String[] args) {
		SpringApplication.run(WebSonosServerApplication.class, args);
	}
	
}
