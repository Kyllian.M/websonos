package com.websonosserver.WebSonosServer.controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.websonosserver.WebSonosServer.domain.*;
import com.websonosserver.WebSonosServer.domain.json.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class IndexController {
	Playlist playlist = null;
	SchedulerJSON scheduler = null;
	Settings settings = null;
	SchedulerTask task = null;
	
	public IndexController() throws JsonParseException, JsonMappingException, IOException {
		super();
		ObjectMapper mapper = new ObjectMapper();
		
		settings = mapper.readValue(new File("settings.json"), Settings.class);
		playlist = new Playlist(settings.getMusic_library_path());
		if(settings.getAutoplay() && playlist.getMusics().size() > 0)
			playlist.play();
		if(settings.getDefault_schedule() != null) {
			scheduler = mapper.readValue(new File("schedules/" + settings.getDefault_schedule()), SchedulerJSON.class);
			task = new SchedulerTask();
			task.start();
		}
	}
	
	@GetMapping("/schedule")
	public SchedulerJSON welcome(Model model){
		return scheduler;
	}
	
	@GetMapping({"/music/{id}","/music"})
	public MusicJSON music(@PathVariable(required=false) String id) {
		if(id != null) {
			if(id.charAt(0) == '-') {
				int val = Integer.parseInt(id.substring(1));
				int index = playlist.getCurrent_index() - val < 0 ? playlist.getMusics().size() - 1 : playlist.getCurrent_index() - val;
				return new MusicJSON(playlist.getMusics().get(index));
			}
			else if (id.charAt(0) == '+') {
				int val = Integer.parseInt(id.substring(1));
				int index = playlist.getCurrent_index() + val >= playlist.getMusics().size() ? 0 : playlist.getCurrent_index() + val;
				return new MusicJSON(playlist.getMusics().get(index));
			}
			else for(Music music : playlist.getMusics()) {
				if(music.getId() == Integer.parseInt(id))
					return new MusicJSON(music);
			}
			return null;
		}
		return new MusicJSON(playlist.getMusics().get(playlist.getCurrent_index()));
	}
	
	@GetMapping("/music/timestamp")
	public TimestampJSON timestamp(Model model) {
		return new TimestampJSON(playlist.getMusics().get(playlist.getCurrent_index()));
	}
	
	@RequestMapping("/command")
	@ResponseBody
	public void handleCommand(@RequestBody RequestJSON input){
		System.out.println("Received JSON From Client : " + input);
		playlist.applyRequest(input);
	}
	
	@RequestMapping("/postschedule")
	@ResponseBody
	public void postSchedule(@RequestBody SchedulerJSON input){
		System.out.println("Received JSON From Client : " + input);
		try {
			ObjectMapper mapper = new ObjectMapper();
			File savefile = new File("schedules/schedule" + Math.random()*10000 + ".json");
			mapper.writeValue(savefile,input);
			settings.setDefault_schedule(savefile.getName());
			mapper.writeValue(new File("settings.json"), settings);
		} catch (IOException e) {
			e.printStackTrace();
		}
		scheduler = input;
	}
	
	@GetMapping("/playlist")
	public PlayListJSON playlist(@RequestParam(required = false) String offset,@RequestParam(required = false) String limit) {
		int off = offset == null ? -1 : Integer.parseInt(offset);
		int lim = limit == null ? -1 : Integer.parseInt(limit);
		return new PlayListJSON(playlist,off,lim);
	}
	
	@RequestMapping("/lookup")
	@ResponseBody
	public List<MusicJSON> lookup(@RequestBody LookupJSON input){
		System.out.println("Received JSON From Client : " + input);
		int lim = input.getLimit();
		ArrayList<MusicJSON> music_list = new ArrayList<>();
		for(Music music : playlist.getMusics()) {	//Menu multi-search
			if(	music.getMetadata().getFilename().toUpperCase().matches(".*"+Pattern.quote(input.getTitle().toUpperCase())+".*") ||
				music.getMetadata().getTitle().toUpperCase().   matches(".*"+Pattern.quote(input.getTitle().toUpperCase())+".*") ||
				music.getMetadata().getArtist().toUpperCase().  matches(".*"+Pattern.quote(input.getTitle().toUpperCase())+".*") ||
				music.getMetadata().getGenre().toUpperCase().   matches(".*"+Pattern.quote(input.getTitle().toUpperCase())+".*")
			) {
				music_list.add(new MusicJSON(music));
				if(lim != -1 && music_list.size() >= lim)
					break;
			}
		}
		return music_list;
	}
	
	private class SchedulerTask {
		long delay = 60 * 1000; // delay in milliseconds	1 minute
	    LoopTask task = new LoopTask();
	    MetadataTask metatask = new MetadataTask();
	    Timer timer = new Timer("UpdateSchedule");
	
	    public void start() {
	        timer.cancel();
	        timer = new Timer("TaskName");
	        Date executionDate = new Date(); // no params = now
	        timer.scheduleAtFixedRate(task, executionDate, delay);
	        timer.schedule(metatask, 500);
	    }
	
	    private class LoopTask extends TimerTask {
	        public void run() {
	        	scheduler.activate(playlist);
	        }
	    }
	    
	    private class MetadataTask extends TimerTask {
	        public void run() {
	        	playlist.updateMetadata();
	        }
	    }
	}
}
